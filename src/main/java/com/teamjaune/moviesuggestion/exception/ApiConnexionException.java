package com.teamjaune.moviesuggestion.exception;

public class ApiConnexionException extends Exception {
    public ApiConnexionException(String errorMessage) {
        super(errorMessage);
    }
}

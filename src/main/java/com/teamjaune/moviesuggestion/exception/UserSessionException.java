package com.teamjaune.moviesuggestion.exception;

public class UserSessionException extends Exception {
    public UserSessionException(String errorMessage) {
        super(errorMessage);
    }
}

package com.teamjaune.moviesuggestion.service;

import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.entity.User;
import com.teamjaune.moviesuggestion.exception.UserSessionException;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private static final String USER_NOT_FOUND = "User with id %s is not authenticated";
    private UserRepository userRepository;

    public User getAuthenticatedUser(Authentication authentication) throws UserSessionException {
        Optional<User> user = userRepository.findById(authentication.getId());
        if(user.isEmpty()) {
            throw new UserSessionException(String.format(USER_NOT_FOUND, authentication.getId()));
        }
        return user.get();
    }

}

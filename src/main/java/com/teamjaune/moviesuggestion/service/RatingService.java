package com.teamjaune.moviesuggestion.service;

import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.controller.rating.RatingResponse;
import com.teamjaune.moviesuggestion.entity.Rating;
import com.teamjaune.moviesuggestion.entity.User;
import com.teamjaune.moviesuggestion.exception.UserSessionException;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RatingService {
    private RatingRepository ratingRepository;
    private UserService userService;

    public RatingResponse update(Rating ratingObj, Integer rating) throws IllegalArgumentException {
        ratingObj.setRating(rating);
        return RatingResponse.parse(ratingRepository.save(ratingObj));
    }

    public RatingResponse create(Authentication authentication, String moveId, int rating) throws UserSessionException {
        User user = userService.getAuthenticatedUser(authentication);
        Rating ratingObj = new Rating(moveId, rating, user);
        return RatingResponse.parse(ratingRepository.save(ratingObj));
    }

    public void delete(Rating rating) {
        ratingRepository.delete(rating);
    }
}

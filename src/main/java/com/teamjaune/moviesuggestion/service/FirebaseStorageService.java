package com.teamjaune.moviesuggestion.service;

import com.google.cloud.storage.Bucket;
import com.google.firebase.cloud.StorageClient;
import lombok.NoArgsConstructor;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.UnsupportedDataTypeException;
import java.io.IOException;

@Service
@NoArgsConstructor
public class FirebaseStorageService {
    private final Bucket bucket = StorageClient.getInstance().bucket();

    public String uploadProfilePhoto(Long userID, MultipartFile file) throws IOException {
            Tika tika = new Tika();
            String mimeType = tika.detect(file.getInputStream());
            var imageName = "profile_" + userID;
            switch (mimeType) {
                case "image/png" :
                case "image/jpeg" :
                    bucket.create(imageName, file.getInputStream());
                    break;
                default: {
                    throw new UnsupportedDataTypeException("File not an image");
                }
            }
        return imageName;
    }

    public byte[] downloadProfilePhoto(Long userID) {
        var blob = bucket.get("profile_" + userID);
        if(blob != null)
            return blob.getContent();
        else
            return bucket.get("placeholder.png").getContent();

    }


}

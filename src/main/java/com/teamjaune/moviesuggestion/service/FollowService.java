package com.teamjaune.moviesuggestion.service;

import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.entity.Follow;
import com.teamjaune.moviesuggestion.entity.User;
import com.teamjaune.moviesuggestion.exception.UserSessionException;
import com.teamjaune.moviesuggestion.repository.FollowRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class FollowService {
    private FollowRepository followRepository;
    private UserService userService;

    public Boolean create(@AuthenticationPrincipal Authentication authentication, User toFollow) throws UserSessionException {
        var currentUser = userService.getAuthenticatedUser(authentication);
        var optFollow = followRepository.findByUser_IdAndFollowing_Id(currentUser.getId(), toFollow.getId());
        if(optFollow == null) {
            Follow follow = new Follow(currentUser, toFollow);
            followRepository.save(follow);
        }
        return true;
    }

    public void delete(@AuthenticationPrincipal Authentication authentication, User toFollow) throws UserSessionException {
        var currentUser = userService.getAuthenticatedUser(authentication);
        var follow = followRepository.findByUser_IdAndFollowing_Id(currentUser.getId(), toFollow.getId());
        if(follow != null) {
            followRepository.delete(follow);
        }
    }
}

package com.teamjaune.moviesuggestion.service;

import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

@Service
public class CommunicationsService {
    @Value( "${movie.server.key}" )
    private String apiKey;
    private static final String ERROR_MSG_API_CONNEXION = "error when connecting with the API on the path %s";

    public HttpResponse<String> sendGetRequest(String endpoint, Map<String, String> params) throws ApiConnexionException {
        try {
            URIBuilder uriBuilder = new URIBuilder(endpoint);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriBuilder.addParameter(entry.getKey(), entry.getValue());
            }
            URI uri = uriBuilder.build();

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .header("Authorization", apiKey)
                    .method("GET", HttpRequest.BodyPublishers.noBody())
                    .build();
            return HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (URISyntaxException | IOException e) {
            throw new ApiConnexionException(String.format(ERROR_MSG_API_CONNEXION, endpoint));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new ApiConnexionException(String.format(ERROR_MSG_API_CONNEXION, endpoint));
        }
    }

    public HttpResponse<String> sendPostRequest(String endpoint, Map<String, String> params, String body) throws ApiConnexionException {
        try {
            URIBuilder uriBuilder = new URIBuilder(endpoint);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriBuilder.addParameter(entry.getKey(), entry.getValue());
            }
            URI uri = uriBuilder.build();

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .header("Authorization", apiKey)
                    .method("POST", HttpRequest.BodyPublishers.ofString(body))
                    .build();

            return HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (URISyntaxException | IOException e) {
            throw new ApiConnexionException(String.format(ERROR_MSG_API_CONNEXION, endpoint));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new ApiConnexionException(String.format(ERROR_MSG_API_CONNEXION, endpoint));
        }
    }
}

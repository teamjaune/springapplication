package com.teamjaune.moviesuggestion;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.IOException;

@SpringBootApplication
public class MoviesuggestionApplication {

	@Value("classpath:teamjaune-projm2-firebase-adminsdk.json")
	Resource serviceAccount;
	public static void main(String[] args) {
		SpringApplication.run(MoviesuggestionApplication.class, args);
	}

	@Bean
	public FirebaseAuth firebaseAuth() throws IOException{

		FirebaseOptions.Builder options = FirebaseOptions.builder();
		options.setCredentials(GoogleCredentials.fromStream(serviceAccount.getURL().openStream()))
		    .setStorageBucket("dev-teamjaune-projm2");

		FirebaseApp.initializeApp(options.build());
		return FirebaseAuth.getInstance();
	}
}

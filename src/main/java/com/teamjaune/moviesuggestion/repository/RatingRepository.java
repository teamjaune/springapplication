package com.teamjaune.moviesuggestion.repository;

import com.teamjaune.moviesuggestion.entity.Rating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RatingRepository extends CrudRepository<Rating, Long> {
    Optional<Rating> findById(long id);

    Optional<Rating> findByUser_IdAndMovieId(long id, String moveId);

    @Query(value = "SELECT * FROM rating ORDER BY rating_rating DESC Limit 10", nativeQuery = true)
    List<Rating> getBestRatedMovies();

    @Query(value = "SELECT * FROM public.rating WHERE user_user_id=:id AND rating_rating > 0", nativeQuery = true)
    List<Rating> getAllRatingsFromUserId(long id);

    @Query(value = "SELECT * FROM public.rating WHERE user_user_id=:id AND rating_rating > 0 LIMIT 10 OFFSET :offset", nativeQuery = true)
    List<Rating> getAllRatingsFromUserIdWithOffset(long id, int offset);

    @Query(value = "SELECT AVG(rating_rating) FROM public.rating WHERE rating_movie_id=:movieId AND rating_rating > 0", nativeQuery = true)
    Double getAverageRatingByMovieId(String movieId);

    @Query(value = "SELECT DISTINCT(rating_movie_id) FROM public.rating WHERE rating_rating > 0", nativeQuery = true)
    List<String> getAllDistinctMovieId();

    @Query(value = "SELECT * FROM public.rating WHERE user_user_id=:id AND rating_rating >= :minimumRatingValue ORDER BY last_modified_date DESC LIMIT 1 OFFSET :offset", nativeQuery = true)
    Rating getLastRatedMovieId(long id, int minimumRatingValue, int offset);

    @Query("select r from Rating r where r.movieId = ?1 and r.user.id in ?2")
    List<Rating> findByMovieIdAndUser_IdIn(String movieId, Collection<Long> ids);

    @Query(value = "SELECT DISTINCT(rating_movie_id) FROM (SELECT rating_movie_id FROM rating INNER JOIN (VALUES :valuesOfFollowedUsersIds) vals(v) ON (rating.user_user_id = v) WHERE rating_rating >= :minimumRatingValue ORDER BY last_modified_date DESC) as subRequest LIMIT :numberOfRatings", nativeQuery = true)
    List<String> getDistinctMoviesFromFollowsLastRatings(int numberOfRatings, int minimumRatingValue, List<Long> valuesOfFollowedUsersIds);
}

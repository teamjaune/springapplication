package com.teamjaune.moviesuggestion.repository;

import com.teamjaune.moviesuggestion.entity.User;
import lombok.NonNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    @NonNull
    List<User> findAll();
    Optional<User> findById(long id);

    Optional<User> findByFirebaseId(String firebaseId);

    @Query(value = "SELECT * FROM \"user\" WHERE user_id != :currentUserId AND lower(user_username) LIKE lower(concat('%', :searchText,'%')) LIMIT 10 OFFSET :offset", nativeQuery = true)
    List<User> searchAll(String searchText, int offset, Long currentUserId);
}

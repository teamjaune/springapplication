package com.teamjaune.moviesuggestion.repository;

import com.teamjaune.moviesuggestion.entity.Follow;
import com.teamjaune.moviesuggestion.entity.Rating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FollowRepository extends CrudRepository<Follow, Long> {
    Optional<Rating> findById(long id);

    Follow findByUser_IdAndFollowing_Id(Long id, Long id1);

    List<Follow> findByUser_Id(Long id);

    List<Follow> findByFollowing_Id(Long id);

    @Query(value = "SELECT * FROM public.follow WHERE following_user_id=:id  LIMIT 10 OFFSET :offset", nativeQuery = true)
    List<Follow> findByFollowing_IdWithOffset(Long id, int offset);

    @Query(value = "SELECT * FROM public.follow WHERE user_user_id=:id  LIMIT 10 OFFSET :offset", nativeQuery = true)
    List<Follow> findByUser_IdWithOffset(Long id, int offset);

    long countByUser_Id(Long id);

    long countByFollowing_Id(Long id);



}

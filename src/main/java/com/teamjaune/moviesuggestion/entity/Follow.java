package com.teamjaune.moviesuggestion.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Follow implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "following_user_id")
    private User following;

    public Follow(User currentUser, User followedUser) {
        this.user = currentUser;
        this.following = followedUser;
    }
}

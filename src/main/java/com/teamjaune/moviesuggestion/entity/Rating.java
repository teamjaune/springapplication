package com.teamjaune.moviesuggestion.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Rating implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rating_id")
    private Integer id;

    @Column(name = "rating_movie_id")
    private String movieId;

    @Column(name = "rating_rating")
    private Integer rating;

    @CreatedDate
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedDate
    @Temporal(value = TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;

    @ManyToOne
    @JoinColumn(name = "user_user_id")
    private User user;

    public Rating(String movieId, int rating, User user) {
        this.movieId = movieId;
        this.rating = rating;
        this.user = user;
    }
}

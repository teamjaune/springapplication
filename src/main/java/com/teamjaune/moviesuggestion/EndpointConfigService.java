package com.teamjaune.moviesuggestion;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EndpointConfigService {

    @Value("${photo.server.address}")
    private String photoServerAddress;

    @Value("${movie.server.address}")
    private String serverAddress;

    @Value("${ai.server.address}")
    public String aiAddress;

    @Value("${ai.admin.server.address}")
    public String aiAdminAddress;

    public String getSearchMoviesEndpoint() {
        return serverAddress + "/Search/Movie";
    }

    public String getSearchPeopleEndpoint() {
        return serverAddress + "/Search/People";
    }

    public String getSearchCompanyEndpoint() {
        return serverAddress + "/Search/Companies";
    }

    public String getMovieEndpoint() {
        return serverAddress + "/Movie";
    }

    public String getPersonEndpoint() {
        return serverAddress + "/Person";
    }

    public String getCompanyEndpoint() {
        return serverAddress + "/Company";
    }

    public String getMoviesEndpoint() {
        return serverAddress + "/Movies";
    }

    public String getAiSuggestionEndpoint() {
        return aiAddress;
    }

    public String getAiAdminCheckAIFilesEndpoint() { return aiAdminAddress  + "/CheckAIFiles"; }

    public String getAiUpdateSimilarityEndpoint() {
        return aiAdminAddress + "/AddNewRatedMovie";
    }

    public String getPhotoServerAddress() {
        return photoServerAddress;
    }

}

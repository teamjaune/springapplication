package com.teamjaune.moviesuggestion.controller;

import com.teamjaune.moviesuggestion.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Authentication {
    private long id;
    private String firebaseId;
    private String username;
    private String email;

    public static Authentication parse(User user) {
        return new Authentication(
                user.getId(),
                user.getFirebaseId(),
                user.getUsername(),
                user.getEmail());
    }
}

package com.teamjaune.moviesuggestion.controller.rating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RatingRequest implements Serializable {
    private String movieId;
    private Integer rating;
}

package com.teamjaune.moviesuggestion.controller.rating;

import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.controller.suggestions.AIAllRatedMovieIdsRequest;
import com.teamjaune.moviesuggestion.entity.Rating;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.exception.UserSessionException;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import com.teamjaune.moviesuggestion.service.RatingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/rating")
public class RatingController {

    private RatingRepository ratingRepository;
    private RatingService ratingService;
    private CommunicationsService communicationsService;
    private EndpointConfigService endpointConfigService;

    @GetMapping("/{movieId}")
    public ResponseEntity<RatingResponse> get(@AuthenticationPrincipal Authentication authentication,
                                       @PathVariable String movieId) {
        Optional<Rating> rating = ratingRepository.findByUser_IdAndMovieId(authentication.getId(), movieId);
        return rating.map(value -> new ResponseEntity<>(RatingResponse.parse(value), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PutMapping
    public ResponseEntity<RatingResponse> set(@AuthenticationPrincipal Authentication authentication,
                                              @RequestBody RatingRequest ratingRequest) {
        if(ratingRequest.getRating() == null || ratingRequest.getMovieId() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<Rating> rating = ratingRepository.findByUser_IdAndMovieId(authentication.getId(), ratingRequest.getMovieId());
        ResponseEntity<RatingResponse> res;
        if(rating.isPresent()) {
            res = new ResponseEntity<>(ratingService.update(rating.get(), ratingRequest.getRating()), HttpStatus.OK);
        }else{
            try {
                res = new ResponseEntity<>(ratingService.create(authentication, ratingRequest.getMovieId(), ratingRequest.getRating()), HttpStatus.CREATED);
            } catch (UserSessionException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        updateIASimilarityFile();
        return res;
    }

    private void updateIASimilarityFile(){
        new Thread(() -> {
            List<String> allDistinctRatings = ratingRepository.getAllDistinctMovieId();
            List<AIAllRatedMovieIdsRequest> distinctMovieIdsRequest = allDistinctRatings.stream().map(movieId -> new AIAllRatedMovieIdsRequest(movieId)).collect(Collectors.toList());
            String json = new Gson().toJson(distinctMovieIdsRequest);
            try{
                communicationsService.sendPostRequest(endpointConfigService.getAiUpdateSimilarityEndpoint(), new HashMap<>(), json);
            }catch (ApiConnexionException ignored) {
            }
        }).start();
    }

    @DeleteMapping("/{movieId}")
    public ResponseEntity<Void> delete(@AuthenticationPrincipal Authentication authentication,
                                       @PathVariable String movieId) {
        Optional<Rating> rating = ratingRepository.findByUser_IdAndMovieId(authentication.getId(), movieId);
        if(rating.isPresent()) {
            ratingService.delete(rating.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

package com.teamjaune.moviesuggestion.controller.rating;

import com.teamjaune.moviesuggestion.entity.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RatingResponse implements Serializable {
    private String movieId;
    private Integer rating;

    public static RatingResponse parse(Rating rating) {
        return new RatingResponse(
                rating.getMovieId(),
                rating.getRating());
    }
}

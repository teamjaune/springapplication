package com.teamjaune.moviesuggestion.controller.crew;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CrewMovieResponse {
    private String movie_id;
    private String title_type;
    private String primary_title;
    private String original_title;
    private Boolean is_adult;
    private Integer start_year;
    private Integer end_year;
    private Integer runtime_minutes;
    private Double average_rating;
    private Integer number_votes;
    private String image_url;
    private String french_title;
    private String synopsis;
    private String person_id;
    private String category;
    private List<String> role_category;
    private String job;
}

package com.teamjaune.moviesuggestion.controller.crew;

import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;
import java.util.HashMap;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/crew")
public class CrewController {

    private CommunicationsService communicationsService;
    private EndpointConfigService endpointConfigService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> get(@PathVariable String id) {
        try {
            HttpResponse<String> response = communicationsService.sendGetRequest(endpointConfigService.getPersonEndpoint() + "/" + id, new HashMap<>());
            CrewResponse crewResponse = new Gson().fromJson(response.body(), CrewResponse.class);
            return new ResponseEntity<>(crewResponse, HttpStatus.OK);
        } catch (ApiConnexionException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

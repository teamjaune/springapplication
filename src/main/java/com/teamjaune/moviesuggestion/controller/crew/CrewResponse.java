package com.teamjaune.moviesuggestion.controller.crew;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CrewResponse {
    private String person_id;
    private String name;
    private Integer birth_year;
    private Integer death_year;
    private String image_url;
    private ArrayList<CrewMovieResponse> movies;
    private String biography;
    private String birth_place;
}

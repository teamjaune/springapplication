package com.teamjaune.moviesuggestion.controller.suggestions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.entity.Rating;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.repository.FollowRepository;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/suggestions")
public class SuggestionsController {

    private CommunicationsService communicationsService;
    private RatingRepository ratingRepository;
    private EndpointConfigService endpointConfigService;
    private FollowRepository followRepository;
    private UserRepository userRepository;

    @GetMapping("/globalSuggestions")
    public ResponseEntity<MovieResponse[]> getGlobalSuggestions(
            @AuthenticationPrincipal Authentication authentication) {
        if (checkForAiFiles()) {
            List<Rating> userRatings = ratingRepository.getAllRatingsFromUserId(authentication.getId());
            if (!userRatings.isEmpty()) {
                List<AIRatingRequest> ratingRequests = userRatings.stream().map(rating -> new AIRatingRequest(rating.getMovieId(), rating.getRating())).collect(Collectors.toList());
                String json = new Gson().toJson(ratingRequests);
                try {
                    HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getAiSuggestionEndpoint(), new HashMap<>(), json);
                    String[] suggestionsResponse = new Gson().fromJson(response.body(), String[].class);
                    ObjectMapper objectMapper = new ObjectMapper();
                    if (suggestionsResponse.length > 0) {
                        MoviesRequest mr = new MoviesRequest(Arrays.asList(suggestionsResponse));
                        HttpResponse<String> response2 = communicationsService.sendPostRequest(endpointConfigService.getMoviesEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(mr));
                        MovieResponse[] moviesResponse = new Gson().fromJson(response2.body(), MovieResponse[].class);
                        return new ResponseEntity<>(moviesResponse, HttpStatus.OK);
                    }
                } catch(ApiConnexionException | JsonProcessingException e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
                List<String> globalRatings = ratingRepository.getBestRatedMovies().stream().map(Rating::getMovieId).collect(Collectors.toList());

                MoviesRequest mr = new MoviesRequest(globalRatings);
                try {
                    HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getMoviesEndpoint(), new HashMap<>(), (new ObjectMapper()).writeValueAsString(mr));
                    MovieResponse[] moviesResponse = new Gson().fromJson(response.body(), MovieResponse[].class);
                    return new ResponseEntity<>(moviesResponse, HttpStatus.OK);
                } catch (ApiConnexionException | JsonProcessingException e) {
                    e.printStackTrace();
                }

            }
    }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
}

    @GetMapping("/followingsSuggestions")
    public ResponseEntity<MovieResponse[]> getFollowingsSuggestions(@AuthenticationPrincipal Authentication authentication) {
        var ourID = authentication.getId();
        var optUser = userRepository.findById(ourID);
        if (optUser.isPresent()) {
            var user = optUser.get();
            var followingIdsList = followRepository.findByUser_Id(user.getId()).stream().map(u -> u.getFollowing().getId()).collect(Collectors.toList());
            if (followingIdsList.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            List<String> lastRatingsFromFollowing = ratingRepository.getDistinctMoviesFromFollowsLastRatings(10, 8, followingIdsList);
            if (lastRatingsFromFollowing.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                MoviesRequest mr = new MoviesRequest(lastRatingsFromFollowing);
                HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getMoviesEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(mr));
                MovieResponse[] moviesResponse = new Gson().fromJson(response.body(), MovieResponse[].class);
                return new ResponseEntity<>(moviesResponse, HttpStatus.OK);
            } catch (ApiConnexionException | JsonProcessingException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/lastRatedSuggestions/{offset}")
    public ResponseEntity<LastRatedSuggestionResponse> getLastRatedSuggestions(
            @AuthenticationPrincipal Authentication authentication, @PathVariable int offset) {

        if (checkForAiFiles()) {
            Rating lastUserRating = ratingRepository.getLastRatedMovieId(authentication.getId(), 8, offset);
            if (lastUserRating == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            LastRatedSuggestionResponse currentSuggestion = getSuggestionForOneMovie(lastUserRating);

            if (currentSuggestion == null) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return new ResponseEntity<>(currentSuggestion, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private LastRatedSuggestionResponse getSuggestionForOneMovie(Rating movieToGetSuggestionsFrom) {
        List<AIRatingRequest> lastRatedRequest = new ArrayList<>();
        lastRatedRequest.add(new AIRatingRequest(movieToGetSuggestionsFrom.getMovieId(), movieToGetSuggestionsFrom.getRating()));
        String json = new Gson().toJson(lastRatedRequest);
        try {
            HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getAiSuggestionEndpoint(), new HashMap<>(), json);
            String[] suggestionsResponse = new Gson().fromJson(response.body(), String[].class);
            ObjectMapper objectMapper = new ObjectMapper();
            if (suggestionsResponse.length > 0) {
                MoviesRequest mr = new MoviesRequest(Arrays.asList(suggestionsResponse));
                HttpResponse<String> response2 = communicationsService.sendPostRequest(endpointConfigService.getMoviesEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(mr));
                MovieResponse[] moviesResponse = new Gson().fromJson(response2.body(), MovieResponse[].class);
                HttpResponse<String> lastRatedResponse = communicationsService.sendGetRequest(endpointConfigService.getMovieEndpoint() + "/" + movieToGetSuggestionsFrom.getMovieId(), new HashMap<>());
                MovieResponse lastRatedMovieResponse = new Gson().fromJson(lastRatedResponse.body(), MovieResponse.class);
                return new LastRatedSuggestionResponse(lastRatedMovieResponse, moviesResponse);
            }
        } catch (ApiConnexionException | JsonProcessingException e) {
            return null;
        }
        return null;
    }

    private boolean checkForAiFiles() {
        List<String> allDistinctRatings = ratingRepository.getAllDistinctMovieId();
        if (allDistinctRatings.isEmpty()) {
            return false;
        }
        List<AIAllRatedMovieIdsRequest> distinctMovieIdsRequest = allDistinctRatings.stream().map(AIAllRatedMovieIdsRequest::new).collect(Collectors.toList());
        String json = new Gson().toJson(distinctMovieIdsRequest);
        try {
            HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getAiAdminCheckAIFilesEndpoint(), new HashMap<>(), json);
            return new Gson().fromJson(response.body(), Boolean.class);
        } catch (ApiConnexionException e) {
            return false;
        }
    }
}

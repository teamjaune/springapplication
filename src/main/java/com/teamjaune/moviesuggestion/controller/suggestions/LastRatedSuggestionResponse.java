package com.teamjaune.moviesuggestion.controller.suggestions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LastRatedSuggestionResponse implements Serializable {
    private MovieResponse last_rated_movie;
    private MovieResponse[] suggestions;
}

package com.teamjaune.moviesuggestion.controller.suggestions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AIRatingRequest {
    private String movie_id;
    private Integer rating;
}

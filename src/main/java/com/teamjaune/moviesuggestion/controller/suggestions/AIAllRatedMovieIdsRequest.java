package com.teamjaune.moviesuggestion.controller.suggestions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AIAllRatedMovieIdsRequest {
    private String movie_id;
}

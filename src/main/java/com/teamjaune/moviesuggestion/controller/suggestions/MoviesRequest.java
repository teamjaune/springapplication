package com.teamjaune.moviesuggestion.controller.suggestions;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class MoviesRequest {
    public final List<String> movieIDs;
}

package com.teamjaune.moviesuggestion.controller.suggestions;

import com.teamjaune.moviesuggestion.controller.movie.MovieCrewResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieResponse implements Serializable {
    private String movie_id;
    private String title_type;
    private String primary_title;
    private String original_title;
    private Boolean is_adult;
    private Integer start_year;
    private Integer end_year;
    private Integer runtime_minutes;
    private Double average_rating;
    private Integer number_votes;
    private String image_url;
    private String french_title;
    private String synopsis;
}

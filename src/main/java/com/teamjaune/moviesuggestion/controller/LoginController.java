package com.teamjaune.moviesuggestion.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.teamjaune.moviesuggestion.entity.User;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/public/login")
public class LoginController {

    private FirebaseAuth firebaseAuth;
    private UserRepository userRepository;

    public static void firebaseTokenToUserDto(FirebaseToken decodedToken, User user) {
        user.setFirebaseId(decodedToken.getUid());
        var name = decodedToken.getName();
        if(name == null || name.length() <= 0){
            var email = decodedToken.getEmail();
            var username = email.split("@")[0];
            name = username.replace("\\.", " ");
        }
        user.setUsername(name);
        user.setEmail(decodedToken.getEmail());
        user.setPicture(decodedToken.getPicture());
        user.setIssuer(decodedToken.getIssuer());
        user.setEnabled(decodedToken.isEmailVerified());
    }

    @GetMapping()
    public ResponseEntity<String> login(@RequestHeader("Authorization") String authHeader) {
        String token = authHeader;
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
            token = token.substring(7);
        }
        try {
            Long userID;
            FirebaseToken decodedToken = firebaseAuth.verifyIdToken(token);
            Optional<User> userOptional = userRepository.findByFirebaseId(decodedToken.getUid());
            if(userOptional.isPresent()) {
                userID = userOptional.get().getId();
            } else {
                User newUser = new User();
                firebaseTokenToUserDto(decodedToken, newUser);
                userID = userRepository.save(newUser).getId();
            }
            return ResponseEntity.ok("{\"userID\": " + userID + "}");

        } catch (FirebaseAuthException authException) {
            return ResponseEntity.status(401).body("Un Authorized");
        }
    }
}

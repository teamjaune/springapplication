package com.teamjaune.moviesuggestion.controller.user.httpobject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileResponse {
    private String username;
    private String picture;
    private Integer movie_count;
    private Long followers_count;
    private Long following_count;
    private Boolean is_followed;
    private Boolean is_current;

}

package com.teamjaune.moviesuggestion.controller.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.controller.user.httpobject.FollowersHttpResponse;
import com.teamjaune.moviesuggestion.controller.user.httpobject.UserMovieResponse;
import com.teamjaune.moviesuggestion.controller.user.httpobject.UserMoviesRequest;
import com.teamjaune.moviesuggestion.controller.user.httpobject.UserProfileResponse;
import com.teamjaune.moviesuggestion.entity.Rating;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.exception.UserSessionException;
import com.teamjaune.moviesuggestion.repository.FollowRepository;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import com.teamjaune.moviesuggestion.service.FirebaseStorageService;
import com.teamjaune.moviesuggestion.service.FollowService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/user")
public class UserController {

    private FollowService followService;
    private FollowRepository followRepository;
    private RatingRepository ratingRepository;
    private UserRepository userRepository;
    private CommunicationsService communicationsService;
    private FirebaseStorageService firebaseStorageService;
    private EndpointConfigService endpointConfigService;

    @GetMapping(value = {"/", "/{id}"})
    public ResponseEntity<Object> get(@AuthenticationPrincipal Authentication authentication, @PathVariable(required = false) Long id) {
        var userID = id == null ? authentication.getId() : id;
        var optUser = userRepository.findById(userID);
        if (optUser.isPresent()) {
            var user = optUser.get();
            var username = user.getUsername();
            var picture = user.getPicture();
            var isCurrent = user.getId() == authentication.getId();
            var moviesRated = user.getRatings().size();
            var followingCount = followRepository.countByUser_Id(user.getId());
            var followersCount = followRepository.countByFollowing_Id(user.getId());
            var isFollowed = followRepository.findByUser_IdAndFollowing_Id(authentication.getId(), user.getId()) != null;
            var response = new UserProfileResponse(username, picture, moviesRated, followersCount, followingCount, isFollowed, isCurrent);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = {"/movies/{offset}", "/{id}/movies/{offset}"})
    public ResponseEntity<Object> getMovies(@AuthenticationPrincipal Authentication authentication, @PathVariable(required = false) Long id, @PathVariable int offset) {
        var userID = id == null ? authentication.getId() : id;
        var ourID = authentication.getId();
        var ourOptUser = userRepository.findById(ourID);
        var optUser = userRepository.findById(userID);
        if (ourOptUser.isPresent() && optUser.isPresent()) {
            try {
                var ourUser = ourOptUser.get();
                var user = optUser.get();
                boolean isCurrentUserOurUser = ourUser.getId().equals(user.getId());
                List<Rating> ourRatings;
                if(isCurrentUserOurUser){
                    ourRatings = ratingRepository.getAllRatingsFromUserIdWithOffset(ourUser.getId(), offset);
                }else{
                    ourRatings = ratingRepository.getAllRatingsFromUserId(ourUser.getId());
                }

                var userRatings = ratingRepository.getAllRatingsFromUserIdWithOffset(user.getId(), offset);
                if (userRatings.isEmpty()) {
                    return new ResponseEntity<>(new UserProfileResponse[0], HttpStatus.OK);
                }
                var movieList = userRatings.stream().map(Rating::getMovieId).collect(Collectors.toList());
                var userMoviesRequestObject = new UserMoviesRequest(movieList);
                var requestBody = new ObjectMapper().writeValueAsString(userMoviesRequestObject);
                HttpResponse<String> movieListResponse = communicationsService.sendPostRequest(endpointConfigService.getMoviesEndpoint(), new HashMap<>(), requestBody);
                UserMovieResponse[] userMovieResponse = new Gson().fromJson(movieListResponse.body(), UserMovieResponse[].class);
                var response = getRatingsResponse(userRatings, ourRatings, userMovieResponse, isCurrentUserOurUser);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (ApiConnexionException | JsonProcessingException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    private Object getRatingsResponse (List<Rating> userRatings, List<Rating> ourRatings, UserMovieResponse[] userMovieResponse, boolean isCurrentUserOurUser){
        return userRatings.stream().map(rating -> {
            var movie = Arrays.stream(userMovieResponse).filter(movieResponse-> movieResponse.getMovie_id().equals(rating.getMovieId())).findFirst().get();
            if(ourRatings != null) {
                Integer ourRating = 0;
                for (Rating r : ourRatings) {
                    if (r.getMovieId().equals(rating.getMovieId())) {
                        ourRating = r.getRating();
                        break;
                    }
                }
                movie.setOur_rating(ourRating);
            }
            movie.setUser_rating(isCurrentUserOurUser ? null : rating.getRating());
            return movie;
        }).collect(Collectors.toList());
    }

    @GetMapping(value = {"/followers/{offset}", "/{id}/followers/{offset}"})
    public ResponseEntity<Object> getFollowers(@AuthenticationPrincipal Authentication authentication, @PathVariable(required = false) Long id, @PathVariable int offset) {
        var userID = id == null ? authentication.getId() : id;
        var ourID = authentication.getId();
        var optUser = userRepository.findById(userID);
        if (optUser.isPresent()) {
            var user = optUser.get();
            var response = followRepository.findByFollowing_IdWithOffset(user.getId(), offset).stream().map(u -> {
                var followingUser = u.getUser();
                var isFollowed = followRepository.findByUser_IdAndFollowing_Id(authentication.getId(), followingUser.getId()) != null;
                var isSelf = followingUser.getId() == ourID;
                return new FollowersHttpResponse(followingUser.getId(), followingUser.getUsername(), followingUser.getPicture(), isFollowed, isSelf);
            }).collect(Collectors.toList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = {"/following/{offset}", "/{id}/following/{offset}"})
    public ResponseEntity<Object> getFollowing(@AuthenticationPrincipal Authentication authentication, @PathVariable(required = false) Long id, @PathVariable int offset) {
        var userID = id == null ? authentication.getId() : id;
        var ourID = authentication.getId();
        var optUser = userRepository.findById(userID);
        if (optUser.isPresent()) {
            var user = optUser.get();
            var response = followRepository.findByUser_IdWithOffset(user.getId(), offset).stream().map(u -> {
                var followedUser = u.getFollowing();
                var isFollowed = followRepository.findByUser_IdAndFollowing_Id(authentication.getId(), followedUser.getId()) != null;
                var isSelf = followedUser.getId() == ourID;
                return new FollowersHttpResponse(followedUser.getId(), followedUser.getUsername(), followedUser.getPicture(), isFollowed, isSelf);
            }).collect(Collectors.toList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}/follow")
    public ResponseEntity<Object> follow(@AuthenticationPrincipal Authentication authentication, @PathVariable Long id) {
        var optUser = userRepository.findById(id);
        if (optUser.isPresent()) {
            var userToFollow = optUser.get();
            try {
                followService.create(authentication, userToFollow);
            } catch (UserSessionException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/{id}/follow")
    public ResponseEntity<Object> unfollow(@AuthenticationPrincipal Authentication authentication, @PathVariable Long id) {
        var optUser = userRepository.findById(id);
        if (optUser.isPresent()) {
            var userToUnFollow = optUser.get();
            try {
                followService.delete(authentication, userToUnFollow);
            } catch (UserSessionException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = {"/", ""})
    public ResponseEntity<Object> updateProfile(@AuthenticationPrincipal Authentication authentication, @RequestParam(required = false) String username, @RequestParam(required = false) MultipartFile file) {
        var optUser = userRepository.findById(authentication.getId());
        if(optUser.isPresent()) {
            var user = optUser.get();
            if (username != null) {
                user.setUsername(username);
            }
            if(file != null) {
                try {
                    firebaseStorageService.uploadProfilePhoto(user.getId(), file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

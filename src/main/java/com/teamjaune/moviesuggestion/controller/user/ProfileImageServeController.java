package com.teamjaune.moviesuggestion.controller.user;

import com.teamjaune.moviesuggestion.service.FirebaseStorageService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/public/images/profile")
public class ProfileImageServeController {
    private FirebaseStorageService firebaseStorageService;
    @GetMapping(value="/{userID}",produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] serveFile(@PathVariable Long userID) {
            // check if the current user has the right to access the user profile (are friends)
            return firebaseStorageService.downloadProfilePhoto(userID);
        }
}

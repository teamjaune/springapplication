package com.teamjaune.moviesuggestion.controller.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserRequest {
    public final Long id;
    public final Integer offset;
}

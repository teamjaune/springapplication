package com.teamjaune.moviesuggestion.controller.user.httpobject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FollowersHttpResponse {
    private Long id;
    private String username;
    private String picture;
    private Boolean is_followed;
    private Boolean is_self;
}

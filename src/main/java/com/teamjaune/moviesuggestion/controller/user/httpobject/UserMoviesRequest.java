package com.teamjaune.moviesuggestion.controller.user.httpobject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserMoviesRequest {
    private List<String> movieIDs;
}

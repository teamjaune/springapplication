package com.teamjaune.moviesuggestion.controller.companies;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyResponse {
    private String company_id;
    private String name;
    private String description;
    private String image_url;
    private String origin_country;
    private String headquarters;
    private String homepage;
    private ArrayList<CompanyMovieResponse> movies;
}

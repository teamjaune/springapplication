package com.teamjaune.moviesuggestion.controller.movie;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieCompanyResponse implements Serializable {
    private String company_id;
    private String name;
}

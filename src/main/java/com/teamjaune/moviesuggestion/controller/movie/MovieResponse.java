package com.teamjaune.moviesuggestion.controller.movie;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieResponse implements Serializable {
    private String movie_id;
    private String title_type;
    private String primary_title;
    private String original_title;
    private Boolean is_adult;
    private Integer start_year;
    private Integer end_year;
    private Integer runtime_minutes;
    private Integer user_rating;
    private Double average_rating;
    private Integer number_votes;
    private Double our_average_rating;
    private String image_url;
    private String french_title;
    private String synopsis;
    private ArrayList<MovieCrewResponse> crew;
    private ArrayList<String> genres;
    private ArrayList<String> keywords;
    private ArrayList<MovieCompanyResponse> production_companies;
    private ArrayList<MovieWatchMethodsResponse> watch_methods;
    private ArrayList<MovieWatchResponseLanguagesResponse> watch_methods_languages;
}

package com.teamjaune.moviesuggestion.controller.movie;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieWatchMethodsResponse implements Serializable {
    private String provider_id;
    private String name;
    private String country;
    private String type;
    private String image_url;
    private Integer display_priority;
}

package com.teamjaune.moviesuggestion.controller.movie;

import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.entity.Follow;
import com.teamjaune.moviesuggestion.entity.Rating;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.repository.FollowRepository;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/movie")
public class MovieController {

    private CommunicationsService communicationsService;
    private RatingRepository ratingRepository;
    private FollowRepository followRepository;
    private EndpointConfigService endpointConfigService;

    @GetMapping("/{id}")
    public ResponseEntity<MovieResponse> get(
            @AuthenticationPrincipal Authentication authentication,
            @PathVariable String id) {
        try {
            HttpResponse<String> response = communicationsService.sendGetRequest(endpointConfigService.getMovieEndpoint() + "/" + id, new HashMap<>());
            MovieResponse movieResponse = new Gson().fromJson(response.body(), MovieResponse.class);
            Optional<Rating> rating = ratingRepository.findByUser_IdAndMovieId(authentication.getId(), movieResponse.getMovie_id());
            rating.ifPresent(value -> movieResponse.setUser_rating(value.getRating()));
            Double ourAverageRating = ratingRepository.getAverageRatingByMovieId(movieResponse.getMovie_id());
            if(ourAverageRating == null) {
                ourAverageRating = 0.0;
            }
            movieResponse.setOur_average_rating(ourAverageRating);
            return new ResponseEntity<>(movieResponse, HttpStatus.OK);
        } catch (ApiConnexionException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/ratings")
    public ResponseEntity<List<MovieUserRatingResponse>> getFollowingRatings(
            @AuthenticationPrincipal Authentication authentication,
            @PathVariable String id) {
        List<Follow> following = followRepository.findByUser_Id(authentication.getId());
        List<Long> ids = following.stream().map(f -> f.getFollowing().getId()).collect(Collectors.toList());
        List<Rating> ratings = ratingRepository.findByMovieIdAndUser_IdIn(id, ids);

        var userMovieRating = ratings.stream().map(rating -> new MovieUserRatingResponse(rating.getUser().getId(), rating.getUser().getUsername(), rating.getUser().getPicture(), rating.getRating())).collect(Collectors.toList());
        return new ResponseEntity<>(userMovieRating, HttpStatus.OK);
    }
}

package com.teamjaune.moviesuggestion.controller.movie;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieCrewResponse implements Serializable {
    private String person_id;
    private String category;
    private String name;
    private Integer birth_year;
    private Integer death_year;
    private String image_url;
}

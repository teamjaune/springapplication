package com.teamjaune.moviesuggestion.controller.movie;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovieUserRatingResponse {
    private Long id;
    private String username;
    private String picture;
    private Integer rating;

}

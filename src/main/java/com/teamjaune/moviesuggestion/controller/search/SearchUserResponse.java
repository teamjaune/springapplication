package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchUserResponse{
    private Long id;
    private String username;
    private String picture;
    private Boolean is_followed;
}

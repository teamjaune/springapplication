package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchCompanyResponse {
    private String company_id;
    private String name;
    private String description;
    private String image_url;
    private String origin_country;
}

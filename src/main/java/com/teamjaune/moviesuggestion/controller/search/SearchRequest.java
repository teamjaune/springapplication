package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SearchRequest {
    public final String searchText;
    public final Integer offset;
}

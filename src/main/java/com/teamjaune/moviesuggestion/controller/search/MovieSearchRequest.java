package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovieSearchRequest implements Serializable {
    private String searchText;
    private List<String> genresToIgnore;
    private Boolean familyFriendly;
    private Integer startYear;
    private Integer endYear;
    private Integer offset;
}

package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchMovieResponse {
    private String movie_id;
    private String title_type;
    private String primary_title;
    private String original_title;
    private Boolean is_adult;
    private Integer start_year;
    private Integer end_year;
    private Integer runtime_minutes;
    private Integer our_rating;
    private Double average_rating;
    private Integer number_votes;
    private String image_url;
    private String french_title;
    private String synopsis;
}

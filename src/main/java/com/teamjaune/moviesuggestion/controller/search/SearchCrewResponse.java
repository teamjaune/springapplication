package com.teamjaune.moviesuggestion.controller.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchCrewResponse {
    private String person_id;
    private String name;
    private Integer birth_year;
    private Integer death_year;
    private String image_url;
    private String biography;
    private String birth_place;
    private Integer cpt;
}

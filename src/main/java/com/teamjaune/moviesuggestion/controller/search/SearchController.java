package com.teamjaune.moviesuggestion.controller.search;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.teamjaune.moviesuggestion.EndpointConfigService;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.exception.ApiConnexionException;
import com.teamjaune.moviesuggestion.repository.RatingRepository;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import com.teamjaune.moviesuggestion.service.CommunicationsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("api/private/search")
public class SearchController {

    private CommunicationsService communicationsService;

    private RatingRepository ratingRepository;

    private UserRepository userRepository;

    private EndpointConfigService endpointConfigService;

    @PostMapping(value = "/movie")
    public ResponseEntity<SearchMovieResponse[]> searchMovies(@AuthenticationPrincipal Authentication authentication, @RequestBody MovieSearchRequest request) {
        ObjectMapper objectMapper = new ObjectMapper();
        String searchText = request.getSearchText() == null ? "" : request.getSearchText();
        List<String> genresToIgnore = request.getGenresToIgnore() == null ? new ArrayList<>() : request.getGenresToIgnore();
        boolean familyFriendly = request.getFamilyFriendly() == null || request.getFamilyFriendly();
        int startYear = request.getStartYear() == null ? 0 : request.getStartYear();
        int endYear = request.getEndYear() == null ? 3000 : request.getEndYear();
        int offset = request.getOffset() == null ? 0 : request.getOffset();

        MovieSearchRequest sr = new MovieSearchRequest(searchText, genresToIgnore, familyFriendly, startYear, endYear, offset);
        try {
            HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getSearchMoviesEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(sr));
            SearchMovieResponse[] searchMovieResponse = new Gson().fromJson(response.body(), SearchMovieResponse[].class);
            //Inject rating
            for (SearchMovieResponse movie : searchMovieResponse) {
                var rating = ratingRepository.findByUser_IdAndMovieId(authentication.getId(), movie.getMovie_id());
                rating.ifPresentOrElse(value -> movie.setOur_rating(value.getRating()), () -> movie.setOur_rating(0));
            }
            return new ResponseEntity<>(searchMovieResponse, HttpStatus.OK);
        } catch (ApiConnexionException | JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/crew")
    public ResponseEntity<SearchCrewResponse[]> searchCrewMembers(@RequestBody SearchRequest request) {
        ObjectMapper objectMapper = new ObjectMapper();
        Integer offset = request.getOffset() == null ? 0 : request.getOffset();
        String searchText = request.getSearchText() == null ? "" : request.getSearchText();
        SearchRequest sr = new SearchRequest(searchText, offset);
        try {
            HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getSearchPeopleEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(sr));
            SearchCrewResponse[] searchCrewResponse = new Gson().fromJson(response.body(), SearchCrewResponse[].class);
            return new ResponseEntity<>(searchCrewResponse, HttpStatus.OK);
        } catch (ApiConnexionException | JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/company")
    public ResponseEntity<SearchCompanyResponse[]> searchCompanies(@RequestBody SearchRequest request) {
        ObjectMapper objectMapper = new ObjectMapper();
        Integer offset = request.getOffset() == null ? 0 : request.getOffset();
        String searchText = request.getSearchText() == null ? "" : request.getSearchText();
        SearchRequest sr = new SearchRequest(searchText, offset);
        try {
            HttpResponse<String> response = communicationsService.sendPostRequest(endpointConfigService.getSearchCompanyEndpoint(), new HashMap<>(), objectMapper.writeValueAsString(sr));
            SearchCompanyResponse[] searchCompaniesResponse = new Gson().fromJson(response.body(), SearchCompanyResponse[].class);
            return new ResponseEntity<>(searchCompaniesResponse, HttpStatus.OK);
        } catch (ApiConnexionException | JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/user")
    public ResponseEntity<List<SearchUserResponse>> searchUsers(@AuthenticationPrincipal Authentication authentication, @RequestBody SearchRequest request) {
        var userID = authentication.getId();
        int offset = request.getOffset() == null ? 0 : request.getOffset();
        String searchText = request.getSearchText() == null ? "" : request.getSearchText();
        var users = userRepository.searchAll(searchText, offset, userID).stream().map(user -> new SearchUserResponse(user.getId(), user.getUsername(), user.getPicture(), false)).collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}

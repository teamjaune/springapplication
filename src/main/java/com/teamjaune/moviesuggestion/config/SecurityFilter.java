package com.teamjaune.moviesuggestion.config;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.teamjaune.moviesuggestion.config.models.Credentials;
import com.teamjaune.moviesuggestion.config.models.SecurityProperties;
import com.teamjaune.moviesuggestion.controller.Authentication;
import com.teamjaune.moviesuggestion.controller.LoginController;
import com.teamjaune.moviesuggestion.entity.User;
import com.teamjaune.moviesuggestion.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@AllArgsConstructor
@Component
@Slf4j
public class SecurityFilter extends OncePerRequestFilter {

	private UserRepository userRepository;
	private SecurityService securityService;
	private CookieService cookieService;
	private SecurityProperties securityProps;
	private FirebaseAuth firebaseAuth;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		authorize(request);
		filterChain.doFilter(request, response);
	}

	private void authorize(HttpServletRequest request) {
		String sessionCookieValue = null;
		FirebaseToken decodedToken = null;
		Credentials.CredentialType type = null;

		Cookie sessionCookie = cookieService.getCookie("session");
		String token = securityService.getBearerToken(request);
		try {
			if (sessionCookie != null) {
				sessionCookieValue = sessionCookie.getValue();
				decodedToken = firebaseAuth.verifySessionCookie(sessionCookieValue,
						securityProps.getFirebaseProps().isEnableCheckSessionRevoked());
				type = Credentials.CredentialType.SESSION;
			} else if (token != null && !token.equals("null")
					&& !token.equalsIgnoreCase("undefined")) {
				decodedToken = firebaseAuth.verifyIdToken(token);
				type = Credentials.CredentialType.ID_TOKEN;
			}
		} catch (FirebaseAuthException e) {
			log.error("Firebase Exception:: " + e.getMessage());
		}

		if (decodedToken == null) {
			return;
		}

		Optional<User> userOptional = userRepository.findByFirebaseId(decodedToken.getUid());
		FirebaseToken finalDecodedToken = decodedToken;
		Authentication userAuthenticated = Authentication.parse(userOptional.stream().findAny().orElseGet(
				() -> {
					User newUser = new User();
					LoginController.firebaseTokenToUserDto(finalDecodedToken, newUser);
					return userRepository.save(newUser);
				}
		));
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
				userAuthenticated,
				new Credentials(type, decodedToken, token, sessionCookieValue),
				null);
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
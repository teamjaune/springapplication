# SpringApplication

## Requirements:
* docker >= 17.12.0+
* docker-compose
* java 11 (for backend)
* npm (for frontend)

## Quick Start
* Clone or download this repository
* Go inside of docker directory
* Run this command `docker-compose up -d`

## For Backend development 
* Open the project using intellij 
* Start `MoviesuggestionApplication` class

## Access to postgres: 
* `localhost:45432`
* **Username:** postgres (as a default)
* **Password:** password (as a default)

## Access to PgAdmin: 
* **URL:** `http://localhost:40080`
* **Username:** admin@admin.com (as a default)
* **Password:** admin (as a default)

## Add a new server in PgAdmin:
* **Host name/address** `postgres`
* **Port** `5432`
* **Username** as `POSTGRES_USER`, by default: `postgres`
* **Password** as `POSTGRES_PASSWORD`, by default `changeme`
